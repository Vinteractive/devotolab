<?php

/**
 * Network admin settings for the page builder.
 *
 * @class FLBuilderMultisiteSettings
 */

final class FLBuilderMultisiteSettings {

    /** 
     * @method init
     */
    static public function init()
    {
        add_action( 'network_admin_menu', 'FLBuilderMultisiteSettings::menu' );
            
        if ( isset( $_REQUEST['page'] ) && $_REQUEST['page'] == 'fl-builder-multisite-settings' ) {
            add_action( 'admin_enqueue_scripts', 'FLBuilderAdminSettings::styles_scripts' );
            FLBuilderAdminSettings::save();
        }
    }

    /** 
     * @method menu
     */
    static public function menu()
    {
	    $title = FLBuilderModel::get_branding();
	    $cap   = 'delete_plugins';
	    $slug  = 'fl-builder-multisite-settings';
	    $func  = 'FLBuilderAdminSettings::render';
	    
        add_submenu_page( 'settings.php', $title, $title, $cap, $slug, $func );
    }
}