<script type="text/javascript">

FLBuilderAdminSettingsStrings = {
    selectFile: '<?php _e('Select File', 'fl-builder'); ?>',
    uninstall: '<?php _e('Please type "uninstall" in the box below to confirm that you really want to uninstall the page builder and all of its data.', 'fl-builder'); ?>'
};

</script>