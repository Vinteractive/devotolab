class CustomPostModuleClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'          => __( 'My Module', 'fl-builder' ),
            'description'   => __( 'A totally awesome module!', 'fl-builder' ),
            'category'      => __( 'Advanced Modules', 'fl-builder' ),
            'dir'           => MY_MODULES_DIR . 'my-module/',
            'url'           => MY_MODULES_URL . 'my-module/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}