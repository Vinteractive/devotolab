<?php

// HIDE ACF Menu
// function remove_acf_menu(){

// $current_user = wp_get_current_user();
//   if ($current_user->user_login!='superadminvin'){
//     remove_menu_page( 'edit.php?post_type=acf' );
//   }
// }
// add_action( 'admin_menu', 'remove_acf_menu', 999 );

// Remove Sucomt Press Menu --- ** Not Working
// function remove_custom_press(){

// $current_user = wp_get_current_user();
//   if ($current_user->user_login!='superadminvin'){
//     remove_menu_page( 'admin.php?page=ct_export' );
//   }
// }
// add_action( 'admin_menu', 'remove_custom_press', 999 );

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

// Classes
require_once 'classes/FLChildTheme.php';

// Actions
add_action('fl_head', 'FLChildTheme::stylesheet');

@ini_set( 'upload_max_size' , '300M' );
@ini_set( 'post_max_size', '300M');
@ini_set( 'max_execution_time', '300' );

// Read More Text
add_filter( 'the_content_more_link', 'modify_read_more_link' );
	function modify_read_more_link() {
	return '<a class="more-link" href="' . get_permalink() . '">Read More</a>';
}