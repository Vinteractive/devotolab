<?php get_header(); ?>

<div class="container">
    <div class="row">
        
        <?php FLTheme::sidebar('left'); ?>
        
        <div class="fl-content <?php FLTheme::content_class(); ?>">
            <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

				<?php $post_type = get_post_type(); ?>
			 	
				<?php if ( "research" == $post_type ) { ?>
					<!-- ===== Research ===== -->
					
					<h1>Research</h1>
					<?php get_template_part('content', 'research'); ?>
					
					<!-- ===== / Research ===== -->
				<?php } elseif ( in_category('news') ) { ?>
					<!-- ===== News ===== -->

					<h1>News</h1>
					<?php get_template_part('content', 'single'); ?>

					<!-- ===== / News ===== -->
				<?php } else {

					get_template_part('content', 'single');

				} ?>
				
				

                
        	<?php endwhile; endif; ?>
        </div>
        
        <?php FLTheme::sidebar('right'); ?>
        
    </div>
</div>

<?php get_footer(); ?>