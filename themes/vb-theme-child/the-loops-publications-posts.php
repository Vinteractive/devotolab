<?php
/**
 * The Loops Template: Publication Posts
 *
 * Display a post title and excerpt
 *
 * The "The Loops Template:" bit above allows this to be selectable
 * from a dropdown menu on the edit loop screen.
 *
 * @package The Loops
 * @since 0.2
 */
?>


<div class="tl-loop">

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>
			
			<div class="row">
				<?php if ( has_post_thumbnail() ) { ?>
				<div class="col col-lg-9">
				<?php } else { ?>
					<div class="col col-lg-12">
				<?php }; ?>
				
					<?php if ( tl_in_widget() ) : ?>
						<h4>
					<?php else : ?>
						<h4 style="font-size: 19px;">
					<?php endif; ?>

							<a href="<?php the_field('publication_link');?>"> <?php the_title(); ?></a>

					<?php if ( tl_in_widget() ) : ?>
						</h4>
					<?php else : ?>
						</h4>
					<?php endif; ?>

					<?php the_content(); ?>
						
					<?php if ( !has_post_thumbnail() ) { ?>
						<a href="<?php the_field('publication_link');?>" style="float: right;"><?php the_field('link_text');?></a>
					<?php }?>
				</div>

				<!-- Thumbnail -->
				<?php if ( has_post_thumbnail() ) { ?>
					<div class="col col-lg-3">
						<br>	
						<a href="<?php the_field('publication_link');?>"><?php the_post_thumbnail(); ?></a>
						<a href="<?php the_field('publication_link');?>" style="float: right; margin-top: 10px;"><?php the_field('link_text');?></a>
					</div>
				<?php } ?>
				<!-- / Thumbnail -->
			</div>

			<div class="fl-module-content fl-node-content">
		        <div class="fl-separator"></div>
		    </div>

		<?php endwhile; ?>

	<?php else : ?>

		<div class="tl-not-found"><?php tl_not_found_text(); ?></div>

	<?php endif; ?>

	<div class="tl-pagination"><?php tl_pagination(); ?></div>

</div>
