<?php
/**
 * The Loops Template: Research Posts - text caption
 *
 * Display a post title and excerpt
 *
 * The "The Loops Template:" bit above allows this to be selectable
 * from a dropdown menu on the edit loop screen.
 *
 * @package The Loops
 * @since 0.2
 */
?>


<div class="tl-loop">

	<h1>Research Areas and Impact</h1>
	<br>

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			

			<div class="row">

				<div class="col col-lg-7">

	<?php if ( tl_in_widget() ) : ?>
					<h4>
				<?php else : ?>
					<h2>
				<?php endif; ?>

						<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>

				<?php if ( tl_in_widget() ) : ?>
					</h4>
				<?php else : ?>
					</h2>
				<?php endif; ?>

					
					<?php the_content(); ?>
					For more information contact <a class="accent" href="Alessandra.Devoto@rhul.ac.uk">Dr Alessandra Devoto</a>

				</div>

				<?php if ( has_post_thumbnail() ) { ?>
					<div class="col col-lg-5 figure">
						<br>	
						<?php the_post_thumbnail(); ?><br><br>
						<!-- <?php echo do_shortcode('[ct id="_ct_textarea_54bfa5a898913"]'); ?></br> -->
						<?php the_field('caption'); ?>
					</div>
				<?php } ?>
			
			</div>
			<!-- /row -->

			<hr>

			<div class="fl-separator"></div>

			<!--  seperator -->
<div class="fl-row fl-row-fixed-width fl-row-bg-none fl-node-54b94ba40c0fb" data-node="54b94ba40c0fb">
	<div class="fl-row-content-wrap">
		<div class="fl-row-content fl-row-fixed-width fl-node-content">
			<div class="fl-col-group fl-node-54b94ba40e411" data-node="54b94ba40e411">
				<div class="fl-col fl-node-54b94ba40e5c5" style="width: 100%;" data-node="54b94ba40e5c5">
					<div class="fl-col-content fl-node-content">
						<div class="fl-module fl-module-separator fl-node-54b94ba40bc2f" data-node="54b94ba40bc2f" data-animation-delay="0.0">
							<div class="fl-module-content fl-node-content">
								<div class="fl-separator"></div>    
							</div>
						</div>        
					</div>
				</div>
			</div>        
		</div>
	</div>
</div>
<!--  / seperator -->

		<?php endwhile; ?>

	<?php else : ?>

		<div class="tl-not-found"><?php tl_not_found_text(); ?></div>

	<?php endif; ?>

	<div class="tl-pagination"><?php tl_pagination(); ?></div>

</div>
