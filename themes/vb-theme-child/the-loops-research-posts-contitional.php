<?php
/**
 * The Loops Template: Research Posts - Conditonal
 *
 * Display a post title and excerpt
 *
 * The "The Loops Template:" bit above allows this to be selectable
 * from a dropdown menu on the edit loop screen.
 *
 * @package The Loops
 * @since 0.2
 */
?>


<div class="tl-loop">

	<?php if ( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php $post_layout = get_field('post_layout'); ?>

			<?php if ( $post_layout == "Default" || $post_layout == "Full Width - image below" ) { ?>
					
					<!-- ====== Full Width - image bottom ====== -->

						<div class="row">

							<div class="col col-lg-12">
								<?php if ( tl_in_widget() ) : ?>
										<h4>
									<?php else : ?>
										<h2>
									<?php endif; ?>

											<?php the_title(); ?>

									<?php if ( tl_in_widget() ) : ?>
										</h4>
									<?php else : ?>
										</h2>
									<?php endif; ?>


									<?php the_content(); ?>
									For more information contact <a class="accent" href="mailto:Alessandra.Devoto@rhul.ac.uk">Dr Alessandra Devoto</a>
							</div>
						</div>
						
						<div style="height: 20px;"></div>

						<div class="row">

							<div class="col col-lg-12">
								<?php if ( has_post_thumbnail() ) { ?>
									
										<br>	
										<?php the_post_thumbnail(); ?><br><br>
										<?php the_field('caption'); ?>
									
								<?php } else { 
										the_field('caption');
								} ?>
							</div>

						</div>
						<!-- / row -->

						<div class="fl-module-content fl-node-content">
					        <div class="fl-separator"></div>
					    </div>


					<!-- ====== / Full Width - image bottom ====== -->

			<?php } elseif ( $post_layout == "2 Columns - image right" ) { ?>
					
					<!-- ====== 2 Column Image Right ====== -->
					
						<div class="row">

							<div class="col col-lg-7">

								<?php if ( tl_in_widget() ) : ?>
									<h4>
								<?php else : ?>
									<h2>
								<?php endif; ?>

										<?php the_title(); ?>
										<!-- <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a> -->

								<?php if ( tl_in_widget() ) : ?>
									</h4>
								<?php else : ?>
									</h2>
								<?php endif; ?>

									
									<?php the_content(); ?>
									For more information contact <a class="accent" href="mailto:Alessandra.Devoto@rhul.ac.uk">Dr Alessandra Devoto</a>

								</div>

								<?php if ( has_post_thumbnail() ) { ?>
									<div class="col col-lg-5 figure">
										<br>	
										<?php the_post_thumbnail(); ?><br><br>
										<?php the_field('caption'); ?>
									</div>
								<?php } ?>
						
						</div>
						<!-- /row -->

						<div class="fl-module-content fl-node-content">
					        <div class="fl-separator"></div>
					    </div>

					<!-- ====== / 2 column - image right ====== -->
			
			<?php }; ?>
			<!-- / end layout if -->


		<?php endwhile; ?>

	<?php else : ?>

		<div class="tl-not-found"><?php tl_not_found_text(); ?></div>

	<?php endif; ?>

	<div class="tl-pagination"><?php tl_pagination(); ?></div>

</div>