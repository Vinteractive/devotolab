<?php

if(!comments_open() && '0' == get_comments_number()) {
    return;
}
if(post_password_required()) {
    return;
}

?>
<div class="fl-comments">

    <?php if(have_comments()) : ?>
	<div class="fl-comments-list">

        <h3 class="fl-comments-list-title">
            <?php comments_number(__('No Comments', 'fl-automator'), __('1 Comment', 'fl-automator'), __('% Comments', 'fl-automator'));?>
        </h3>
	
		<ol id="comments">
		<?php wp_list_comments(array('callback' => 'FLTheme::display_comment')); ?>
		</ol>
	
        <?php if(get_comment_pages_count() > 1) : ?>
		<nav class="fl-comments-list-nav clearfix">
			<div class="fl-comments-list-prev"><?php previous_comments_link() ?></div>
			<div class="fl-comments-list-next"><?php next_comments_link() ?></div>
		</nav>
		<?php endif; ?>
		
	</div>
    <?php endif; ?>

    <?php if ($post->comment_status == 'open') : ?>
	<div id="respond">
	
		<h3 class="fl-comments-respond-title"><?php _e('Leave a Comment', 'fl-automator'); ?></h3>
	
		<?php if(get_option('comment_registration') && !$user_ID ) : ?>
		
			<p><?php _e('You must be', 'fl-automator'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in', 'fl-automator'); ?></a> <?php _e('to post a comment.', 'fl-automator'); ?></p>
			
		<?php else : ?>
		
			<form class="fl-comment-form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
			
			<?php if ($user_ID) : ?>
			
				<p><?php _e('Logged in as', 'fl-automator'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'fl-automator'); ?>"><?php _e('Log out', 'fl-automator'); ?> &raquo;</a></p>
				
			<?php else : ?>
			
				<label for="author"><?php _e('Name', 'fl-automator'); ?> <?php if ($req) _e('(required)', 'fl-automator'); ?></label>
				<input type="text" name="author" class="form-control" value="<?php echo $comment_author; ?>" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
				<br />
				
				<label for="email"><?php _e('Email (will not be published)', 'fl-automator'); ?> <?php if ($req) _e('(required)', 'fl-automator'); ?></label>
				<input type="text" name="email" class="form-control" value="<?php echo $comment_author_email; ?>" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
				<br />
				
				<label for="url"><?php _e('Website', 'fl-automator'); ?></label>
				<input type="text" name="url" class="form-control" value="<?php echo $comment_author_url; ?>" tabindex="3" />
				<br />
			
			<?php endif; ?>
			
				<label for="comment"><?php _e('Comment', 'fl-automator'); ?></label>
    			<textarea name="comment" class="form-control" cols="60" rows="8" tabindex="4"></textarea>
    			<br />
    			
    			<input name="submit" type="submit" class="btn btn-primary" tabindex="5" value="<?php _e('Submit Comment', 'fl-automator'); ?>" />
    			<?php comment_id_fields(); ?>
    			<?php do_action('comment_form', $post->ID); ?>
    			
    			<div class="fl-comment-form-cancel">
    				<?php cancel_comment_reply_link(); ?>
    			</div>
			
			</form>
		<?php endif;?>
	</div>
    <?php endif; ?>
</div>