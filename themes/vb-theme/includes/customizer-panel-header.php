<?php
    
/* Header Panel */
FLCustomizer::add_panel('fl-header', array(
    'title'         => __('Header', 'fl-automator'),
    'sections'      => array(
        
        /* Top Bar Layout Section */
        'fl-topbar-layout' => array(
            'title'     => __('Top Bar Layout', 'fl-automator'),
            'options'   => array(
                
                /* Top Bar Layout */
                'fl-topbar-layout' => array(
                    'setting'   => array(
                        'default'   => 'none'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Layout', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'none'          => __('None', 'fl-automator'),
                            '1-col'         => __('1 Column', 'fl-automator'),
                            '2-cols'        => __('2 Columns', 'fl-automator')
                    	)
                    )
                ),
                
                /* Line */
                'fl-topbar-line1' => array(
                    'control'   => array(
                        'class'         => 'FLCustomizerControl',
                    	'type'          => 'line'
                    )
                ),
                
                /* Top Bar Column 1 Layout */
                'fl-topbar-col1-layout' => array(
                    'setting'   => array(
                        'default'   => 'text'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Column 1 Layout', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'text'              => __('Text', 'fl-automator'),
                            'text-social'       => __('Text &amp; Social Icons', 'fl-automator'),
                            'menu'              => __('Menu', 'fl-automator'),
                            'menu-social'       => __('Menu &amp; Social Icons', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator')
                    	)
                    )
                ),
                
                /* Top Bar Column 1 Text */
                'fl-topbar-col1-text' => array(
                    'setting'   => array(
                        'default'   => '',
                        'transport' => 'postMessage'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Column 1 Text', 'fl-automator'),
                    	'type'          => 'textarea'
                    )
                ),
                
                /* Line */
                'fl-topbar-line2' => array(
                    'control'   => array(
                        'class'         => 'FLCustomizerControl',
                    	'type'          => 'line'
                    )
                ),
                
                /* Top Bar Column 2 Layout */
                'fl-topbar-col2-layout' => array(
                    'setting'   => array(
                        'default'   => 'menu'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Column 2 Layout', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'text'              => __('Text', 'fl-automator'),
                            'text-social'       => __('Text &amp; Social Icons', 'fl-automator'),
                            'menu'              => __('Menu', 'fl-automator'),
                            'menu-social'       => __('Menu &amp; Social Icons', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator')
                    	)
                    )
                ),
                
                /* Top Bar Column 2 Text */
                'fl-topbar-col2-text' => array(
                    'setting'   => array(
                        'default'   => '',
                        'transport' => 'postMessage'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Column 2 Text', 'fl-automator'),
                    	'type'          => 'textarea'
                    )
                )
            )
        ),
        
        /* Top Bar Background Section */
        'fl-topbar-bg' => array(
            'title'     => __('Top Bar Background', 'fl-automator'),
            'options'   => array(
                
                /* Top Bar Background Type */
                'fl-topbar-bg-type' => array(
                    'setting'   => array(
                        'default'   => 'content'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Background Type', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'none'          => __('None', 'fl-automator'),
                            'content'       => __('Same as Content', 'fl-automator'),
                            'custom'        => __('Custom', 'fl-automator')
                    	)
                    )
                ),
                
                /* Top Bar Background Color */
                'fl-topbar-bg-color' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Color_Control',
                    	'label'     => __('Top Bar Background Color', 'fl-automator')
                    )
                ),
                
                /* Top Bar Background Gradient */
                'fl-topbar-bg-gradient' => array(
                    'setting'   => array(
                        'default'   => '0'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Top Bar Background Gradient', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            '0'             => __('Disabled', 'fl-automator'),
                            '1'           	=> __('Enabled', 'fl-automator')
                    	)
                    )
                )
            )
        ),
    
        /* Header Layout Section */
        'fl-header-layout' => array(
            'title'     => __('Header Layout', 'fl-automator'),
            'options'   => array(
                
                /* Header Layout */
                'fl-header-layout' => array(
                    'setting'   => array(
                        'default'   => 'right'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Header Layout', 'fl-automator'),
                    	'type'      => 'select',
                    	'choices'   => array(
                            'none'      => __('None', 'fl-automator'),
                            'bottom'    => __('Navigation Bottom', 'fl-automator'),
                            'right'     => __('Navigation Right', 'fl-automator'),
                            'centered'  => __('Navigation Centered', 'fl-automator')
                    	)
                    )
                ),

                /* Header Padding */
                'fl-header-padding' => array(
                    'setting'   => array(
                        'default'           => '30',
                        'sanitize_callback' => 'FLCustomizer::sanitize_number'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                    	'label'     => __('Header Padding (px)', 'fl-automator'),
                    	'type'      => 'text'
                    )
                ),
                
                /* Fixed Header */
                'fl-fixed-header' => array(
                    'setting'   => array(
                        'default'   => 'visible'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Fixed Header', 'fl-automator'),
                        'description'   => __('Show a fixed header as the page is scrolled.', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'hidden'        => __('Disabled', 'fl-automator'),
                            'visible'       => __('Enabled', 'fl-automator')
                    	)
                    )
                ),
                
                /* Header Search */
                'fl-header-nav-search' => array(
                    'setting'   => array(
                        'default'   => 'visible'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Navigation Search Icon', 'fl-automator'),
                    	'type'      => 'select',
                    	'choices'   => array(
                        	'visible'	=> __('Enabled', 'fl-automator'),
                        	'hidden'	=> __('Disabled', 'fl-automator')
                    	)
                    )
                ),
                
                /* Line */
                'fl-header-line1' => array(
                    'control'   => array(
                        'class'         => 'FLCustomizerControl',
                    	'type'          => 'line'
                    )
                ),
                
                /* Header Content Layout */
                'fl-header-content-layout' => array(
                    'setting'   => array(
                        'default'   => 'social-text'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Header Content Layout', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'none'              => __('None', 'fl-automator'),
                            'text'              => __('Text', 'fl-automator'),
                            'social'            => __('Social Icons', 'fl-automator'),
                            'social-text'       => __('Text &amp; Social Icons', 'fl-automator')
                    	)
                    )
                ),
                
                /* Header Content Text */
                'fl-header-content-text' => array(
                    'setting'   => array(
                        'default'   => 'Call Us! 1-800-555-5555'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Header Content Text', 'fl-automator'),
                    	'type'      => 'textarea'
                    )
                )
            )
        ),
    
        /* Header Background Section */
        'fl-header-bg' => array(
            'title'     => __('Header Background', 'fl-automator'),
            'options'   => array(
                
                /* Header Background Type */
                'fl-header-bg-type' => array(
                    'setting'   => array(
                        'default'   => 'content'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Header Background Type', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'none'          => __('None', 'fl-automator'),
                            'content'       => __('Same as Content', 'fl-automator'),
                            'custom'        => __('Custom', 'fl-automator')
                    	)
                    )
                ),
                
                /* Header Background Color */
                'fl-header-bg-color' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Color_Control',
                    	'label'     => __('Header Background Color', 'fl-automator')
                    )
                ),
                
                /* Header Background Gradient */
                'fl-header-bg-gradient' => array(
                    'setting'   => array(
                        'default'   => '0'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Header Background Gradient', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            '0'             => __('Disabled', 'fl-automator'),
                            '1'           	=> __('Enabled', 'fl-automator')
                    	)
                    )
                )
            )
        ),
    
        /* Header Logo Section */
        'fl-header-logo' => array(
            'title'     => __('Header Logo', 'fl-automator'),
            'options'   => array(
                
                /* Logo Type */
                'fl-logo-type' => array(
                    'setting'   => array(
                        'default'   => 'text'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Logo Type', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'text'          => __('Text', 'fl-automator'),
                            'image'         => __('Image', 'fl-automator')
                    	)
                    )
                ),

                /* Logo Image (Regular) */
                'fl-logo-image' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Image_Control',
                        'label'     => __('Logo Image (Regular)', 'fl-automator')
                    )
                ),

                /* Logo Image (Retina) */
                'fl-logo-image-retina' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Image_Control',
                        'label'         => __('Logo Image (Retina)', 'fl-automator')
                    )
                ),
                
                /* Logo Text */
                'fl-logo-text' => array(
                    'setting'   => array(
                        'default'   => get_bloginfo('name'),
                        'transport' => 'postMessage'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Logo Text', 'fl-automator'),
                    	'type'          => 'text'
                    )
                ),
                
                /* Logo Font Family */
                'fl-logo-font-family' => array(
                    'setting'   => array(
                        'default'   => 'Helvetica',
                        'transport' => 'postMessage'
                    ),
                    'control'   => array(
                        'class'     => 'FLCustomizerControl',
                        'label'     => __('Logo Font Family', 'fl-automator'),
                    	'type'      => 'font',
                    	'connect'   => 'fl-logo-font-weight'
                    )
                ),
                
                /* Logo Font Weight */
                'fl-logo-font-weight' => array(
                    'setting'   => array(
                        'default'   => '400'
                    ),
                    'control'   => array(
                        'class'     => 'FLCustomizerControl',
                        'label'     => __('Logo Font Weight', 'fl-automator'),
                    	'type'      => 'font-weight',
                    	'connect'   => 'fl-logo-font-family'
                    )
                ),

                /* Logo Font Size */
                'fl-logo-font-size' => array(
                    'setting'   => array(
                        'default'   => '30',
                        'transport' => 'postMessage',
                        'sanitize_callback' => 'FLCustomizer::sanitize_number'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                    	'label'     => __('Logo Font Size (px)', 'fl-automator'),
                    	'type'      => 'text'
                    )
                )
            )
        ),
        
        /* Navigation Background Section */
        'fl-nav-bg' => array(
            'title'     => __('Navigation Background', 'fl-automator'),
            'options'   => array(
                
                /* Navigation Background Type */
                'fl-nav-bg-type' => array(
                    'setting'   => array(
                        'default'   => 'content'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Navigation Background Type', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'none'          => __('None', 'fl-automator'),
                            'content'       => __('Same as Content', 'fl-automator'),
                            'custom'        => __('Custom', 'fl-automator')
                    	)
                    )
                ),
                
                /* Navigation Background Color */
                'fl-nav-bg-color' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Color_Control',
                    	'label'     => __('Navigation Background Color', 'fl-automator')
                    )
                ),
                
                /* Navigation Background Gradient */
                'fl-nav-bg-gradient' => array(
                    'setting'   => array(
                        'default'   => '0'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Navigation Background Gradient', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            '0'             => __('Disabled', 'fl-automator'),
                            '1'           	=> __('Enabled', 'fl-automator')
                    	)
                    )
                )
            )
        ),
    
        /* Nav Font Section */
        'fl-nav-font' => array(
            'title'     => __('Navigation Text', 'fl-automator'),
            'options'   => array(
                
                /* Navigation Text Type */
                'fl-nav-text-type' => array(
                    'setting'   => array(
                        'default'   => 'default'
                    ),
                    'control'   => array(
                        'class'         => 'WP_Customize_Control',
                        'label'         => __('Navigation Text Type', 'fl-automator'),
                    	'type'          => 'select',
                    	'choices'       => array(
                            'default'       => __('Default', 'fl-automator'),
                            'custom'        => __('Custom', 'fl-automator')
                    	)
                    )
                ),
                
                /* Nav Font Family */
                'fl-nav-font-family' => array(
                    'setting'   => array(
                        'default'   => 'Helvetica',
                        'transport' => 'postMessage'
                    ),
                    'control'   => array(
                        'class'     => 'FLCustomizerControl',
                        'label'     => __('Navigation Font Family', 'fl-automator'),
                    	'type'      => 'font',
                    	'connect'   => 'fl-nav-font-weight'
                    )
                ),
                
                /* Nav Font Weight */
                'fl-nav-font-weight' => array(
                    'setting'   => array(
                        'default'   => '400'
                    ),
                    'control'   => array(
                        'class'     => 'FLCustomizerControl',
                        'label'     => __('Navigation Font Weight', 'fl-automator'),
                    	'type'      => 'font-weight',
                    	'connect'   => 'fl-nav-font-family'
                    )
                ),
                
                /* Nav Font Format */
                'fl-nav-font-format' => array(
                    'setting'   => array(
                        'default'   => 'none',
                        'transport' => 'postMessage'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Navigation Font Format', 'fl-automator'),
                    	'type'      => 'select',
                    	'choices'   => array(
                            'none'       => __('Regular', 'fl-automator'),
                            'capitalize' => __('Capitalize', 'fl-automator'),
                            'uppercase'  => __('Uppercase', 'fl-automator'),
                            'lowercase'  => __('Lowercase', 'fl-automator')
                    	)
                    )
                ),

                /* Nav Font Size */
                'fl-nav-font-size' => array(
                    'setting'   => array(
                        'default'   => '16',
                        'transport' => 'postMessage',
                        'sanitize_callback' => 'FLCustomizer::sanitize_number'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                    	'label'     => __('Navigation Font Size (px)', 'fl-automator'),
                    	'type'      => 'text'
                    )
                )
            )
        ),
    )
));