<?php
    
/* Settings Panel */
FLCustomizer::add_panel('fl-settings', array(
    'title'         => __('Settings', 'fl-automator'),
    'sections'      => array(
        
        /* Social Links Section */
        'fl-social-links' => array(
            'title'     => __('Social Links', 'fl-automator'),
            'options'   => array(
                
                /* Social Icons Color */
                'fl-social-icons-color' => array(
                    'setting'   => array(
                        'default'   => 'mono'
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Social Icons Color', 'fl-automator'),
                        'type'      => 'select',
                        'choices'   => array(
                            'branded'   => __('Branded', 'fl-automator'),
                            'mono'      => __('Monochrome', 'fl-automator')
                        )
                    )
                ),

                /* Social Links */
                'fl-social-facebook' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Facebook', 'fl-automator')
                    )
                ),
                'fl-social-twitter' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Twitter', 'fl-automator')
                    )
                ),
                'fl-social-google' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Google', 'fl-automator')
                    )
                ),
                'fl-social-linkedin' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('LinkedIn', 'fl-automator')
                    )
                ),
                'fl-social-yelp' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Yelp', 'fl-automator')
                    )
                ),
                'fl-social-pinterest' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Pinterest', 'fl-automator')
                    )
                ),
                'fl-social-tumblr' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Tumblr', 'fl-automator')
                    )
                ),
                'fl-social-vimeo' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Vimeo', 'fl-automator')
                    )
                ),
                'fl-social-youtube' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('YouTube', 'fl-automator')
                    )
                ),
                'fl-social-flickr' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Flickr', 'fl-automator')
                    )
                ),
                'fl-social-instagram' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Instagram', 'fl-automator')
                    )
                ),
                'fl-social-dribbble' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Dribbble', 'fl-automator')
                    )
                ),
                'fl-social-500px' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('500px', 'fl-automator')
                    )
                ),
                'fl-social-blogger' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Blogger', 'fl-automator')
                    )
                ),
                'fl-social-github' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('GitHub', 'fl-automator')
                    )
                ),
                'fl-social-rss' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('RSS', 'fl-automator')
                    )
                ),
                'fl-social-email' => array(
                    'control'   => array(
                        'class'     => 'WP_Customize_Control',
                        'label'     => __('Email', 'fl-automator')
                    )
                )
            )
        ),
    
        /* Favicons Section */
        'fl-favicons' => array(
            'title'     => __('Favicons', 'fl-automator'),
            'options'   => array(

                /* Favicon */
                'fl-favicon' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Image_Control',
                        'label'     => __('Favicon', 'fl-automator')
                    )
                ),

                /* Apple Touch Icon */
                'fl-apple-touch-icon' => array(
                    'setting'   => array(
                        'default'   => ''
                    ),
                    'control'   => array(
                        'class'     => 'WP_Customize_Image_Control',
                        'label'     => __('Apple Touch Icon', 'fl-automator')
                    )
                )
            )
        ),
    )
));