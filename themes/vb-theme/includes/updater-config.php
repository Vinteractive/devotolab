<?php

if(class_exists('FLUpdater')) {
    FLUpdater::add_product(array(
        'name'      => 'Beaver Builder Theme', 
        'version'   => '1.2.1', 
        'slug'      => 'bb-theme',
        'type'    	=> 'theme'
    ));
}